#include "UnitTest.h"

#include <iostream>
#include <chrono>

// template is much faster than specialized one ???
template<class Int>
bool isPrim( Int n )
{
    for ( Int i = 2; i < n; i++ )
    {
        if ( n % i == 0 )
        {
            return false;
        }
    }
    return true;
}

// template this function will slow down it.
size_t time_sume_primes( size_t upper )
{
    size_t sum = 0;
    for ( int i = 2; i < upper; i++ )
    {
        if ( isPrim( i ) )
            sum += i;
    }
    return sum;
}

TEST_CASE( "primenumbers" )
{
    size_t upper = 100000;

    // std::cout << "Input a number (default " << upper << "): ";
    // std::string line;
    // std::getline( std::cin, line );
    // if ( !line.empty() )
    // {
    //     if ( size_t x = std::strtoul( line.c_str(), nullptr, 10 ) )
    //         upper = x;
    // }

    auto   t1   = std::chrono::high_resolution_clock::now();
    size_t sum  = time_sume_primes( upper );
    auto   t2   = std::chrono::high_resolution_clock::now();
    auto   diff = t2 - t1;
    int    ms   = std::chrono::duration_cast<std::chrono::milliseconds>( diff ).count();
    std::cout << "*** Cpp primenumbers time(ms): " << ms << ", upper_bound: " << upper << ", sum_result: " << sum << std::endl;
}
