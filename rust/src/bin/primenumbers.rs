use std::time::Instant;
// use std::io;

fn main(){
    let upper : usize = 100000;

    // let mut line = String::new();
    // println!("Input a number (defualt {}):", upper);
    // io::stdin().read_line(&mut line).expect("input an integer");
    // upper = line.trim().parse().unwrap_or(upper);

    let t1 = Instant::now();
    let sum = time_sum_prim(upper);
    let t2 = Instant::now();
    let diff = t2-t1;
    println!("*** Rust Prime numbers time(ms): {}, upper_bound: {}, sum_result: {}", diff.as_millis(), upper, sum);
}

fn time_sum_prim(upper: usize) -> usize {
    let mut sum = 0;
    
    for i in 2..upper{
        if is_prim(i){
            sum += i;
        }
    }
    sum
}
fn is_prim(x: usize) -> bool{
    for i in 2..x{
        if x%i == 0 {
            return false;
        }
    }
    true
}

